import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit  {
  title = 'form-control';

  isChecked=false;
  value="Se selecciono";
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }


  constructor(private _formBuilder: FormBuilder){
    
  }

  onChangeCheck($event){

    console.log($event);
    this.isChecked=!this.isChecked;
  }

  onRadioButtonChange($event){

    console.log($event);
    this.isChecked=!this.isChecked;
  }

  onSelectChange($event){

    console.log($event);
    console.info("Favorite food: "+ $event.value);
  }


}
